/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoImpl;

import Dao.DaoCliente;
import Pojo.Cliente;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sistemas07
 */
public class DaoCLienteImpl implements DaoCliente{
        
    private File fDataC;
    private File fHeaderC;
    private RandomAccessFile rafDataC;
    private RandomAccessFile rafHeaderC;
    private final int SIZE = 500;

    public DaoCLienteImpl() {
    }
    
    // Abrir flujo
    private void open() throws IOException {        
        fHeaderC = new File("clientesheader.dat");
        fDataC = new File("clientes.dat");
        if(!fHeaderC.exists()){
            fHeaderC.createNewFile();
            fDataC.createNewFile();
            rafHeaderC= new RandomAccessFile(fHeaderC, "rw"); 
            rafDataC= new RandomAccessFile(fDataC, "rw");
            rafHeaderC.seek(0);
            rafHeaderC.writeInt(0);
            rafHeaderC.writeInt(0);
        }else{
            rafHeaderC= new RandomAccessFile(fHeaderC, "rw"); 
            rafDataC= new RandomAccessFile(fDataC, "rw");
        }
    }
    
    // Cerrar flujo
    private void close() throws IOException {
        if(rafHeaderC != null) {
            rafHeaderC.close();
        }
        if(rafDataC != null) {
            rafDataC.close();
        }
    }

    // Metodo buscar por id
    @Override
    public Cliente findById(int id) throws IOException {
        open();
        rafHeaderC.seek(4);
        int k = rafHeaderC.readInt();

        if(id>k || id<1){
            return null;
        }
        long posData = (id - 1) * SIZE;
        rafDataC.seek(posData);
        
        Cliente c = new  Cliente();
        c.setId(rafDataC.readInt());
        c.setNombres(rafDataC.readUTF());
        c.setApellidos(rafDataC.readUTF());
        c.setNombreEmpresa(rafDataC.readUTF());
        c.setTipoPersona(rafDataC.readUTF());
        c.setTelefono(rafDataC.readUTF());
        c.setCed(rafDataC.readUTF());
        c.setCorreo(rafDataC.readUTF());
        c.setDireccion(rafDataC.readUTF());

        close();
        
        return c; 
    }

    
    // Metodo para visualizar todos los cleintes
    @Override
    public List<Cliente> getAll() throws IOException {
        open();         
        List<Cliente> departamentos = new ArrayList<>();
        rafHeaderC.seek(0);
        int n = rafHeaderC.readInt();
        int k = rafHeaderC.readInt();
        
        for(int i =0; i<n; i++){
            long posHeader = 8 + (4 * i);
            rafHeaderC.seek(posHeader);
            int id= rafHeaderC.readInt();
            
            long posData = (id - 1) * SIZE;
            rafDataC.seek(posData);
            
            Cliente c = new  Cliente();
            c.setId(rafDataC.readInt());
            c.setNombres(rafDataC.readUTF());
            c.setApellidos(rafDataC.readUTF());
            c.setNombreEmpresa(rafDataC.readUTF());
            c.setTipoPersona(rafDataC.readUTF());
            c.setTelefono(rafDataC.readUTF());
            c.setCed(rafDataC.readUTF());
            c.setCorreo(rafDataC.readUTF());
            c.setDireccion(rafDataC.readUTF());
            
            departamentos.add(c);
        }
        close();
        return departamentos;
    }
    
    // Crear un cliente
    @Override
    public void create(Cliente t) throws IOException {
        open();
        rafHeaderC.seek(0);
        int n = rafHeaderC.readInt();
        int k = rafHeaderC.readInt();
        
        long posData = k * SIZE;
        rafDataC.seek(posData);
        rafDataC.writeInt(k+1);
        rafDataC.writeUTF(t.getNombres());
        rafDataC.writeUTF(t.getApellidos());
        rafDataC.writeUTF(t.getNombreEmpresa());
        rafDataC.writeInt(t.getNatural());
        if(t.getNatural() == 1){
            t.setTipoPersona("Natural");
        }else if(t.getNatural() == 2){
            t.setTipoPersona("Juridica");
        }
        rafDataC.writeUTF(t.getTelefono());
        rafDataC.writeUTF(t.getCed());
        rafDataC.writeUTF(t.getCorreo());
        rafDataC.writeUTF(t.getDireccion());
        
        
        long posHeader = 8 + (4 * k);
        
        rafHeaderC.seek(0);
        rafHeaderC.writeInt(++n);
        rafHeaderC.writeInt(++k);
        
        rafHeaderC.seek(posHeader);
        rafHeaderC.writeInt(k);
        
        close();
    }

    @Override
    public int update(Cliente t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Cliente t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
