/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Pojo;


/**
 *
 * @author Sistemas07
 */
public class Cliente {
    int id;
    String nombres;
    String apellidos;
    String nombreEmpresa;
    int natural;
    String telefono;
    String ced;
    String correo;
    String direccion;
    String tipoPersona;

    public Cliente() {
    }

    public Cliente(String nombres, String apellidos, String nombreEmpresa, String tipoPersona, String telefono, String ced, String correo, String direccion) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nombreEmpresa = nombreEmpresa;
        this.tipoPersona = tipoPersona;
        this.telefono = telefono;
        this.ced = ced;
        this.correo = correo;
        this.direccion = direccion;
    }

    public Cliente(int id, String nombres, String apellidos, String nombreEmpresa, int natural, String telefono, String ced, String correo, String direccion) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nombreEmpresa = nombreEmpresa;
        this.natural = natural;
        this.telefono = telefono;
        this.ced = ced;
        this.correo = correo;
        this.direccion = direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public int getNatural() {
        return natural;
    }

    public void setNatural(int natural) {
        this.natural = natural;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCed() {
        return ced;
    }

    public void setCed(String ced) {
        this.ced = ced;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    

    

    @Override
    public String toString() {
        return "\n__________________________________________________________"
                + "Cliente:\n" +
                "Id:" + id + "\n"+
                "Nombres: " + nombres + "\n" +
                "Apellidos:" + apellidos +  "\n" +
                "Nombre de la empresa:" + nombreEmpresa +  "\n" +
                "Tipo de Persona:" + tipoPersona +  "\n" +
                "Telefono:" + telefono +  "\n" +
                "Ced:" + ced +  "\n" +
                "Correo:" + correo +  "\n" +
                "Direccion:" + direccion + "\n" +
                "__________________________________________________________\n";
    }
    
    
}
