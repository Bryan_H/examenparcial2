/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Pojo.Cliente;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Sistemas07
 */
public interface DaoCliente extends Dao<Cliente> {
    Cliente findById(int id) throws IOException;
}
