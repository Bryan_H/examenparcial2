/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import DaoImpl.DaoCLienteImpl;
import Pojo.Cliente;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Sistemas07
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        Cliente c = new Cliente();
        DaoCLienteImpl daoC = new DaoCLienteImpl();
        int opc; boolean flag = false;
        
        do {
            menu();
            opc = sc.nextInt();
            
            switch(opc){
                case 1: // Registro de clientes
                    c = new Cliente("Bryan","Hernandez","UNI","Natural","888888", "00001-002-22", "bryan@gmail.com","De la casa roja una cuadra arriba");
                    
//                    sc.nextLine(); // Limpiando el buffer
//                    System.out.print("\nIngrese el o los nombres del cliente: ");
//                    c.setNombres(sc.nextLine());
//                    
//                    System.out.print("\nIngrese el o los apellidos del cliente: ");
//                    c.setApellidos(sc.nextLine());
//                    
//                    System.out.print("\nIngrese del nombre de la empresa en que trabaja el cleinte: ");
//                    c.setNombreEmpresa(sc.nextLine());
//                    
//                    int opc2; boolean flag2 = false;
//                    
//                    do {
//                        System.out.print("\nQue tipo de persona es el cliente?\n"
//                                + "1. Persona Natural.\n"
//                                + "2. Persona Juridica.\n"
//                                + "\nSeleccione una opcion: ");
//                        opc2 = sc.nextInt();
//                        
//                        switch(opc2){
//                            case 1: //Natural
//                                c.setNatural(opc2);
//                                flag2 = true;
//                            break;
//                            
//                            case 2: //Juridica
//                                c.setNatural(opc2);
//                                flag2 = true;
//                            break;
//                            
//                            default:
//                                System.out.println("Opcion incorrecta!");
//                        }
//                    } while (!flag2);
//                    
//                    sc.nextLine();
//                    System.out.print("\nIngrese el telefono del cliente: ");
//                    c.setTelefono(sc.nextLine());
//                    
//                    System.out.print("\nIngrese la cedula del cliente: ");
//                    c.setCed(sc.nextLine());
//                    
//                    System.out.print("\nIngrese el correo del cliente: ");
//                    c.setCed(sc.nextLine());
//                    
//                    System.out.print("\nIngrese la direccion del cliente: ");
//                    c.setCorreo(sc.nextLine());
                    
                    daoC.create(c);
                    
                break;
                
                case 2: // Visualizar por Id
                    Scanner in = new Scanner(System.in);
                    System.out.print("\nIngrese el nombre del departamento que desea buscar: ");
                    int id = in.nextInt();
                     if(daoC.findById(id)== null){
                         System.out.println("No se han encontrado coincidencias");
                         break;
                    }
                    System.out.println("\nCliente encontrado: \n"+ daoC.findById(id).toString());                    
                break;
                
                case 3: // Visualizar todos
                     List<Cliente> cli = daoC.getAll();
                     cli.forEach((e) -> {
                        System.out.println(e.toString());
                     });
                break;
                
                case 4: // Salir
                    System.out.println("\nVuelva pronto =D");
                    flag = true;
                break;
                
                default:
                    System.err.println("Opcion invalida!");
            }
            
        } while (!flag);
        
    }
    
    static void menu(){
        System.out.print("Menu de opciones: \n"
                + "1. Regristro de clientes.\n"
                + "2. Visualizar por Id\n"
                + "3. Visualizar todos.\n"
                + "4. Salir.\n"
                + "\nSeleccione una opcion: ");
    }
    
}
